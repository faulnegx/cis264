#include <stdio.h>
#include <string.h>


#define array_length(arr) (sizeof(arr) == 0 ? 0 : sizeof(arr)/sizeof((arr)[0])) 

char * strtok1(char *, const char *);
unsigned int strspn1(const char*, const char*);
unsigned int strcspn1(const char*, const char*);
int strpos1(const char *, const char);

int arr[] = {0, 4, 5, 7, 2, 10, 1, 3};

int main(int argc,char* argv[])
{
    char * sample = "Let's Break This String";
    printf("%d\n", strpos1(sample, 'g'));
    // char * piece = strtok(sample, " ");
    unsigned int val = strcspn1(" . This", " .");
    
    // val = strcspn1("tsere . This", " .");
    printf("%d\n", val);
    // printf("%s", piece);
    return 0;
}

char * strtok1(char * text, const char * delim)
{
    static char* pNextToken;
    if (!text) {
        text = pNextToken;
    }
    text +=strspn1(text, delim);

    if (*text == '\0') return NULL;
    pNextToken = text + strspn1(text, delim);
    if (*pNextToken != '\0') {
        *pNextToken++ = '\0';
    }
    return text;
}

unsigned int strspn1(const char * str, const char * delim) {
    unsigned int pos = 0;
    for (pos = 0; str[pos] != '\0'; ++pos) {
        if (strpos1(delim, str[pos]) == -1) {
            return pos;
        }
    }
    return pos;
}

unsigned int strcspn1(const char * str, const char * delim) {
    unsigned int pos = 0;
    for (pos = 0; str[pos] != '\0'; ++pos) {
        if (strpos1(delim, str[pos]) != -1) {
            return pos;
        }
    }
    return pos;
}

int strpos1(const char * str, const char ch) {
    int found = 0;
    while (str[found] != '\0') {
        if (str[found] == ch) {
            return found;
        }
        ++found;
    }
    return -1;
}