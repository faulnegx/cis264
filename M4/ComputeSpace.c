#include <stdio.h>
#include <limits.h>


int spaceRequired(void) {
    int quant = 0;
    char type = '\0';
    int sum = 0;
    scanf("%d %c", &quant, &type);
    if (type == 'i') {
        sum = sizeof(int) * quant;
    } else if (type == 'c') {
        sum = sizeof(char) * quant;
    } else if (type == 'd') {
        sum = sizeof(double) * quant;
    } else {
        sum = INT_MAX;
    }
    return sum;
}

int main(void)
{
    int trackingCodes = 0;
    scanf("%d", &trackingCodes);
    int totalSpace = 0;
    int space = 0;
    for (int i=0; i < trackingCodes; ++i) {
        space = spaceRequired();
        if (space == INT_MAX) {
            printf("Invalid tracking code type");
            return 0;
        }
        totalSpace += space;
    }
    printf("%d bytes\n", totalSpace);
    return 0;
}
