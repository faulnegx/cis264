#include <stdio.h>


#define array_length(arr) (sizeof(arr) == 0 ? 0 : sizeof(arr)/sizeof((arr)[0])) 

void shift_element(int*);
void insertion_sort(void);

int arr[] = {0, 4, 5, 7, 2, 10, 1, 3};

int main(int argc,char* argv[])
{
    for (int i = 0; i < array_length(arr); ++i) {
        printf("%d ", arr[i]);
    }

    putchar('\n');
    insertion_sort();
    for (int i = 0; i < array_length(arr); ++i) {
        printf("%d ", arr[i]);
    }
    putchar('\n');

    return 0;
}

void insertion_sort(void) {
    for (int i = 1; i < array_length(arr); ++i) {
        if (*(arr+i) < *(arr+i-1)) {
            shift_element(arr+i);
        }
    }
}


void shift_element(int *pElement) {
    int ivalue;
    for (ivalue = *pElement; (pElement > arr) && *(pElement-1) > ivalue; --pElement) {
        *pElement = *(pElement-1);
    }
    *pElement = ivalue;
}