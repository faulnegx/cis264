#include <stdio.h>

void reverseArray(char array[][40], int);

int main(void) {
    int wordMaxLength = 40;
    int essayLength = 68;
    char essay[essayLength][wordMaxLength];
    
    
    for (int i=0; i<essayLength; i++) {
        scanf("%s", essay[i]);
    }

    reverseArray(essay, essayLength);
    puts("\n");
    for (int i=0; i< essayLength; i++) {
        printf("%s ", essay[i]);
    }
    puts("\n");
    return 0;
}

void reverseArray(char inputArray[][40], int arraySize) {
    for (int i = 0;i < arraySize/2; ++i) {
        char temp[40];
        for (int j = 0; j < 40; ++j) {
            temp[j] = inputArray[arraySize-1-i][j];
        }
        for (int j=0; j < 40; ++j) {
            inputArray[arraySize-1-i][j] = inputArray[i][j];
        }
        for (int j=0; j < 40; ++j) {
            inputArray[i][j] = temp[j];
        }
    }
}
