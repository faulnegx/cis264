#include <stdio.h>
#include <stdlib.h>

struct point{
	int x;
	int y;
};

void printPoint(struct point);
void printPoly(struct point *, int);
void initializePoly(struct point *, int);

int main(void) {
    
    // Fill in your main function here
    int vertices;
    scanf("%d", &vertices);
    struct point *polygon;
    polygon = (struct point *) malloc (vertices * sizeof(struct point));
    initializePoly(polygon, vertices);
    printPoly(polygon, vertices);
    free(polygon);
    return 0;
}

void printPoint(struct point pt) {
    printf("(%d, %d)\n", pt.x, pt.y);
}

void printPoly(struct point *ptr, int N) {
    int i;
    for (i=0; i<N; i++) {
        printPoint(ptr[i]);
    }
}

// Write your initializePoly() function here
void initializePoly(struct point * polygonPtr, int vertices) {
    for (int i = 0; i < vertices; ++i) {
        (polygonPtr+i)->x = -1 * i;
        (polygonPtr+i)->y = i * i;
    }
}

