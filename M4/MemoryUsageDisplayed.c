#include <stdio.h>
#include <limits.h>


long long int spaceRequired(void) {
    int quant = 0;
    char type = '\0';
    long long int sum = 0;
    scanf("%c %d", &type, &quant);
    if (type == 'i') {
        sum = sizeof(int) * quant;
    } else if (type == 's') {
        sum = sizeof(short) * quant;
    } else if (type == 'c') {
        sum = sizeof(char) * quant;
    } else if (type == 'd') {
        sum = sizeof(double) * quant;
    } else {
        sum = INT_MAX;
    }
    return sum;
}

void printHumanReadableBytes(long long int totalBytes) {
    int megaBytes = (int) totalBytes / 1000000;
    int kiloBytes = ((int) totalBytes / 1000) - megaBytes * 1000;
    int bytes = (int) totalBytes % 1000;
    if (megaBytes > 0) {
        printf("%d MB and ", megaBytes);
    }
    if (kiloBytes > 0) {
        printf("%d KB and ", kiloBytes);
    }
    printf("%d B\n", bytes);
}

int main(void)
{
    long long int totalSpace = 0;
    long long int space = 0;
    space = spaceRequired();
    if (space == INT_MAX) {
        printf("Invalid tracking code type");
        return 0;
    }
    totalSpace += space;
    printHumanReadableBytes(totalSpace);
    return 0;
}
