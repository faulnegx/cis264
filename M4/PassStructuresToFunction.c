#include <stdio.h>

struct date {
   int year;
   int month;
   int day;
};

void readDate(struct date *);
void printDate(struct date);

int main(void) {
	struct date today;
	readDate(&today);
	printDate(today);
	return 0;
}

void readDate(struct date * dateptr) {
   int inputVal[3];
   for (int i = 0; i < 3; ++i) {
      scanf("%d", inputVal+i);
   }
   dateptr->year = inputVal[0];
   dateptr->month = inputVal[1];
   dateptr->day = inputVal[2];
}

void printDate(struct date instance) {
   printf("%0.2d/%0.2d/%d\n", instance.month, instance.day, instance.year);
}