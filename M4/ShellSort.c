#include <stdio.h>


#define array_length(arr) (sizeof(arr) == 0 ? 0 : sizeof(arr)/sizeof((arr)[0])) 

void shift_element(int*);
void shift_element_by_gap(unsigned int, unsigned int);
void shift_element_by_gap1(int*, unsigned int);
void insertion_sort(void);
void shell_sort(void);

int arr[] = {99, 4, 5, 7, 2, 10, 1, 3};

int main(int argc,char* argv[])
{
    for (int i = 0; i < array_length(arr); ++i) {
        printf("%d ", arr[i]);
    }

    putchar('\n');

    shift_element_by_gap(7, 2);
    shell_sort();
    for (int i = 0; i < array_length(arr); ++i) {
        printf("%d ", arr[i]);
    }
    putchar('\n');

    return 0;
}

void insertion_sort(void) {
    for (int i = 1; i < array_length(arr); ++i) {
        if (*(arr+i) < *(arr+i-1)) {
            shift_element(arr+i);
        }
    }
}


void shift_element(int *pElement) {
    int ivalue;
    for (ivalue = *pElement; (pElement > arr) && *(pElement-1) > ivalue; --pElement) {
        *pElement = *(pElement-1);
    }
    *pElement = ivalue;
}

void shift_element_by_gap1(int *pElement, unsigned int gap) {
    int ivalue;
    for (ivalue = *pElement; (pElement-gap >= arr) && *(pElement-gap) > ivalue; pElement -= gap) {
        *pElement = *(pElement-gap);
    }
    *pElement = ivalue;
}

void shift_element_by_gap(unsigned int i, unsigned int gap) {
    int ivalue;
    for (ivalue = arr[i]; (i >= gap) && (ivalue < arr[i-gap]); i -= gap) {
        arr[i] = arr[i-gap];
    }
    arr[i] = ivalue;
}

void shell_sort(void) {
    unsigned int gap, i, len = array_length(arr);
    for (gap = len/2; gap > 0; gap /= 2) {
        for (i = gap; i < len; ++i) {
            if (arr[i-gap] > arr[i]) {
                shift_element_by_gap(i, gap);
            }
        }
    }
}
