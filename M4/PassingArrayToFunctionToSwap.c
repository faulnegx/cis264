#include <stdio.h>

//Write your function prototype here
void reverseArray(int[]);

int main(void){
    int size = 6;
    int inputArray[size];
    for (int i = 0; i < size; ++i) {
        scanf("%d", &inputArray[i]);
    }
    reverseArray(inputArray);
    for (int i = 0; i < size; ++i) {
        printf("%d ", inputArray[i]);
    }
    puts("");
	return 0;
}

void reverseArray(int inputArray[]) {
    int size = 6;
    for (int i = 0;i < size/2; ++i) {
        int temp = inputArray[size-1-i];
        inputArray[size-1-i] = inputArray[i];
        inputArray[i] = temp;
    }
}
