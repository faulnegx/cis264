#include <stdio.h>

//Write your function prototype here
void predictAge(int*);

int main(void){
	int age;
	int *ageAddr = &age;
	scanf("%d", ageAddr);
	printf("Your current age is %d.\n", age);

	//Write your function call here
    predictAge(ageAddr);

	printf("Your new age will be %d!\n", age);
	return 0;
}


//Write your function here
void predictAge(int *ageAddr) {
    if (*ageAddr <=20) {
        *ageAddr = *ageAddr * 2;
    } else {
        *ageAddr = *ageAddr - 10;
    }
}
