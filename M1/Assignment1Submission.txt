Problem 1.1
a. curly brace denotes a block or compound statement. It makes sense to use curly braces to surround the body of a function to group together multiple statements. 
b. 7 is an integer, "7" is a string-literal and '7' is a char. They are different data type in C program. 
c. double ans = 10.0 + 2.0/((3.0-2.0)*2.0)

Problem 1.2
1. 12.0
2. 3.6
3. 18.0
4. 2.0

Problem 1.3
#include <stdio.h>

int main(void)
{
    puts("hello, 6.087 students");
    return 0;
}

Problem 1.4
6
4
5
2
7
1
3

Problem 1.5
a. no semicolon on preprocessor directive
#include <stdio.h>

b. void means no parameters accepted but we wish to have arg1 to be an parameter. So we will use int instead of void. 
int function(int arg1)
{
  return arg1-1;
}

c. assignment operator/equal sign is not correct syntax for define directive. There should not be an equal size.
#define MESSAGE "Happy new year!"
puts(MESSAGE);
