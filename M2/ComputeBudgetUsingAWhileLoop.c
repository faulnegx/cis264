#include <stdio.h>

int main(void){
    int input = 0;
    scanf("%d", &input);
    int sum = 0;
    while (input != -1){
        sum += input;
        scanf("%d", &input);
    }
    printf("%d\n", sum);
	return 0;
}
