#include <stdio.h>

int main(void){
    char name[51];
    scanf("%s", &name);
    int count = 0;
    while (name[count] != '\0') {
        ++count;
    }
    printf("%d\n", count%2+1);
	return 0;
}
