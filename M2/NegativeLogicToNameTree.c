#include <stdio.h>
#include <string.h>

int main(void){
    int height = 0;
    int leaflets = 0;
    scanf("%d", &height);
    scanf("%d", &leaflets);
    char treeType[20];
    if (height <= 5 && leaflets >= 8) {
        strcpy(treeType,"Tinuviel");
    } else if (height >= 10 && leaflets >= 10) {
        strcpy(treeType,"Calaelen");
    } else if (height <= 8 && leaflets <= 5) {
        strcpy(treeType,"Falarion");
    } else if (height >= 10 && leaflets <= 7) {
        strcpy(treeType,"Dorthonion");
    } else {
        strcpy(treeType,"Uncertain");
    }
    printf("%s\n", treeType);
	return 0;
}
