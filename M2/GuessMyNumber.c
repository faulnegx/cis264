#include <stdio.h>
#include <limits.h>

int main(void){
    int secret = 1;
    scanf("%d", &secret);
    int userInput = INT_MAX;
    int guess = 0;
    while (userInput != secret) {
        scanf("%d", &userInput);
        ++guess;
        if (userInput < secret) {
            puts("it is more");
        } else if (userInput > secret) {
            puts("it is less");
        }
    }
    printf("Number of tries needed:\n%d\n", guess);
	return 0;
}
