#include <stdio.h>

int main(void){
    int size = 0;
    scanf("%d", &size);
    int team1Weights[size];
    int team2Weights[size];
    int team1TotalWeight = 0;
    int team2TotalWeight = 0;
    for (int i = 0; i < size; ++i) {
        scanf("%d", &team1Weights[i]);
        team1TotalWeight += team1Weights[i];
        scanf("%d", &team2Weights[i]);
        team2TotalWeight += team2Weights[i];
    }
    printf("Team %d has an advantage\n", team1TotalWeight>team2TotalWeight ? 1 : 2);
    printf("Total weight for team 1: %d\n", team1TotalWeight);
    printf("Total weight for team 2: %d\n", team2TotalWeight);
	return 0;
}
