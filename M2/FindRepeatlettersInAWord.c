#include <stdio.h>

int main(void){
    char text[51];
    scanf("%s", text);
    char swap;
    int max = 50;
    int lenOfText = 0;
    for (int j = 0; j < 51; ++j) {
        if (text[j] == '\0') {
            break;
        }
        ++lenOfText;
    }
    int temp = lenOfText;
    for (int i = 0; i < temp-1; temp--) {
        for (int j = 0;j < temp-1;++j) {
            if (text[j] > text[j+1]) {
                swap = text[j];
                text[j] = text[j+1];
                text[j+1] = swap;
            }
        }
    }
    int counter = 0;
    for (int i = 0; i < lenOfText; ++i) {
        if (text[i] == text[i+1]){
            ++counter;
        }
        while (text[i] == text[i+1] && text[i+1] != '\0') {
            ++i;
        }
    }
    printf("%d", counter);
	return 0;
}
