#include <stdio.h>

int main(void){
    int size = 0;
    scanf("%d", &size);
    double weights[size];
    double totalWeight = 0;
    for (int i = 0; i < size; ++i) {
        scanf("%lf", &weights[i]);
        totalWeight += weights[i];
    }
    for (int i = 0; i < size; ++i) {
        printf("%.1lf\n", totalWeight/size-weights[i]);
    }
	return 0;
}
