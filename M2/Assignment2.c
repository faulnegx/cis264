#include <stdio.h>
#include <limits.h>
#include <float.h>

void displayBits(int val);

int main(void){
    printf("char: %d\n", sizeof(char));
    printf("unsigned char: %d\n", sizeof(unsigned char));
    printf("short: %d\n", sizeof(short));
    printf("int: %d\n", sizeof(int));
    printf("unsigned int: %d\n", sizeof(unsigned int));
    printf("unsigned long: %d\n", sizeof(unsigned long));
    printf("float: %d\n", sizeof(float));
    printf("double: %d\n", sizeof(double));

    printf("char: %d\n", SCHAR_MAX);
    printf("unsigned char: %d\n", UCHAR_MAX);
    printf("short: %d\n", SHRT_MAX);
    printf("int: %d\n", INT_MAX);
    printf("unsigned int: %lld\n", UINT_MAX);
    printf("unsigned long: %llu\n", ULONG_MAX);
    printf("float: %f\n", FLT_MAX);
    printf("double: %d\n", sizeof(double));

    char test = ' ';
    if (test >='0' && test <='9') {
        printf("%c is a digit\n", test);
    } else if (test >='A' && test <= 'Z') {
        printf("%c is an upper case letter\n", test);
    } else if (test >='a' && test <='z') {
        printf("%c is an lower case letter\n", test);
    } else if (test == ' ' || test == '\t' || test == '\n') {
        printf("%c is a whitespace\n", test);
    } else {
        printf("%c is not a digit, upper/lower case letter, or whitespace \n", test);
    }

    int val = 0xCAFE;
    // displayBits(val);
    // int val = 0xA;
    int maskLast4Bits = 0xF;
    int LSB4 = (val & maskLast4Bits & 1)
        + (val>>1 & maskLast4Bits & 1)
        + (val>>2 & maskLast4Bits & 1)
        + (val>>3 & maskLast4Bits & 1);
    
    printf("0x%X\n", val);
    unsigned int reverseTemp = (val & 0xFF);
    int reverseVal = (reverseTemp << 8) | val >> 8;
    printf("0x%X", reverseVal);
    putchar('\n');

    printf("0x%X\n", val);
    unsigned int rotateTemp = (val & 0xF);
    int rotateVal = (rotateTemp << 12) | val >> 4;
    printf("0x%X\n", rotateVal);
    int x = 10;
    int y = 2;
    int z = 2;
    // z=y=x++ + ++y*2;
    y = y+1; z= (y= (x + (y*2)));x = x+1;
    printf("%d\n", x);
    printf("%d\n", y);
    printf("%d\n", z);


    x = 10;
    y = 4;
    z = 1;
    // y>>=x&0x2 && z;

    printf("%d\n", y >>= ((x&0x2) && z));

    int alliszero=1;
    x = 0;
    y = 0;
    alliszero=(x=1) && (y=0);
    printf("%d\n", alliszero);

    x = 10;
    y = 3;
    z = 0;
    y=++x+y;z=z-->x;
    printf("y = %d\n", y);

    printf("z = %d\n", z);
	return 0;
}

void displayBits(int value)
{
    // define displayMask and left shift 31 bits 
    unsigned int displayMask = 1 << 31;
    printf("%15d = ", value);
    // loop through bits from the left
    for (int c = 1; c <= 32; ++c) 
    { 
        putchar(value & displayMask ? '1' : '0');
        value <<= 1; // shift mask right by 1
        if (c % 8 == 0) { // output space after 8 bits 
            putchar(' ');
        }
    } 
    putchar('\n');
}