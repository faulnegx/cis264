#include <stdio.h>

int main(void){
    int count = 0;
    char text[101];
    scanf("%d", &count);

    int maxLenOfText = 0;
    int lenOfText = 0;
    for (int i = 0; i < count; ++i) {
        scanf("%s", text);
        lenOfText = 0;
        for (int j = 0; j < 101; ++j) {
            if (text[j] == '\0') {
                break;
            }
            ++lenOfText;
        }
        if (lenOfText > maxLenOfText) {
            maxLenOfText = lenOfText;
        }
    }

    printf("%d\n", maxLenOfText);
    
	return 0;
}
