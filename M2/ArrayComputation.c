#include <stdio.h>

int main(void){
    int size = 10;
    scanf("%d", &size);
    double pricePerPound[size];
    double pound[size];
    for (int i = 0; i < size; ++i) {
        scanf("%lf", &pricePerPound[i]);
    }
    for (int i = 0; i < size; ++i) {
        scanf("%lf", &pound[i]);
    }
    double totalCost = 0.0;
    for (int i = 0; i < size; ++i) {
        totalCost += pricePerPound[i] * pound[i];
    }
    printf("%lf\n", totalCost);
	return 0;
}
