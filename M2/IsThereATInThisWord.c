#include <stdio.h>

int main(void){
    char text[51];
    scanf("%s", text);
    int tIndex = -1;
    int lenOfText = 0;
    for (int i = 0; i < 51; ++i) {
        if (text[i] == 'T' || text[i] == 't') {
            tIndex = i;
        }
        if (text[i] == '\0') {
            break;
        }
        ++lenOfText;
    }
    if (tIndex != -1) {
        if (tIndex < lenOfText/2+1) {
            printf("%d\n", 1);
        } else {
            printf("%d\n", 2);
        }
    } else {
        printf("%d\n", tIndex);
    }
    
	return 0;
}
