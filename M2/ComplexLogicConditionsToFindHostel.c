#include <stdio.h>

int main(void){
    int age = 0;
    int luggageWeight = 0;
    scanf("%d", &age);
    scanf("%d", &luggageWeight);
    int price = 0;
    if (age < 10) {
        price = 5;
    } else if (age != 60) {
        if (luggageWeight > 20) {
            price = 30 + 10;
        } else {
            price = 30;
        }
    }
    printf("%d\n", price);
	return 0;
}
