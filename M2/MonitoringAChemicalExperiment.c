#include <stdio.h>
#include <limits.h>

int main(void){
    int min = 1;
    int max = INT_MAX;
    scanf("%d", &min);
    scanf("%d", &max);
    int temperature = max;
    int alert = 0;
    while (temperature !=-999) {
        scanf("%d", &temperature);
        if (!alert){
            if (temperature <= max && temperature >= min) {
                printf("%s\n", "Nothing to report ");
            } else {
                puts("Alert!\n");
                alert = 1;
            }
        }
    }

	return 0;
}
