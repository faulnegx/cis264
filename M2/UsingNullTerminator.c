#include <stdio.h>

int main(void){
    int count = 0;
    scanf("%d", &count);
    char firstName[101];
    char lastName[101];

    for (int i = 0; i < count; ++i) {
        scanf("%s %s", firstName, lastName);
        printf("%s %s\n", lastName, firstName);
    }
    
	return 0;
}
