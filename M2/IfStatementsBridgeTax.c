#include <stdio.h>

int main(void){
    int size = 2;
    int inputArray[size];
    for (int i = 0; i < size; ++i) {
        scanf("%d", &inputArray[i]);
    }
    if (inputArray[0] + inputArray[1] >= 10 ) {
       printf("Special tax\n%d\n", 36);
    } else {
       printf("Regular tax\n%d\n", 2*(inputArray[0] + inputArray[1]));
    }
	return 0;
}
