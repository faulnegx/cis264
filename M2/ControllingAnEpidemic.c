#include <stdio.h>

int main(void){
    int totolPopulation = 1;
    scanf("%d", &totolPopulation);
    int infectedPopulation = 1;
    int days = 1;
    while (infectedPopulation < totolPopulation) {
        infectedPopulation += infectedPopulation * 2;
        ++days;
    }
    printf("%d\n", days);
	return 0;
}
