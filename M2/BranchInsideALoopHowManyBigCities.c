#include <stdio.h>

int main(void){
    int size = 2;
    scanf("%d", &size);
    int populations[size];
    int bigCityCount = 0;
    for (int i = 0; i < size; ++i) {
        scanf("%d", &populations[i]);
        if (populations[i] > 10000) {
            ++bigCityCount;
        }
    }
    printf("%d\n", bigCityCount);
	return 0;
}
