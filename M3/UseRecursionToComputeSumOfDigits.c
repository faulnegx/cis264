#include <stdio.h>
#include <limits.h>

int sumOfDigits(int);

int main(void){
    int value;
    scanf("%d", &value);
    printf("%d\n", sumOfDigits(value));
    return 0;
}

int sumOfDigits(int val) {
    if (val < 10) {
        return val;
    } else {
        return val% 10 + sumOfDigits(val/10);
    }
}