#include <stdio.h>

int main(void){
    int count = 0;
    scanf("%d", &count);
    double value = 0;
    char unit = '\0';
    double meterToFeet = 3.2808;
    double gramToPound = 0.002205;
    
    for (int i = 0; i < count; ++i) {
        scanf("%lf %c", &value, &unit);
        if (unit == 'm') {
            printf("%.6f ft\n", value*meterToFeet);
        } else if (unit == 'g') {
            printf("%.6f lbs\n", value*gramToPound);
        } else if (unit == 'c') {
            printf("%.6f f\n", 32+1.8*value);
        }
    }

    return 0;
}