#include <stdio.h>
#include <limits.h>

int min(int, int);

int main(void){
    int count = 0;
    scanf("%d", &count);
    int value;
    int smallest = INT_MAX;
    for (int i = 0; i < count; ++i) {
        scanf("%d", &value);
        smallest = min(smallest, value);
    }
    printf("%d\n", smallest);

    return 0;
}

int min(int val1, int val2) {
    return val1<val2 ? val1 : val2;
}