#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char* argv[])
{
    FILE* fp=NULL;
    fp=fopen("stateoutflow0708.txt","r");
    if (fp == NULL) {
        puts("cannot open stateoutflow0708.txt");
        exit(-1);
    }
    int numMatch = 0;
    int width = 25;
    // Header
    {
        printf("%-*s%s\n", width, "STATES", "TOTAL");
        printf("---------------------------------\n");
    }
    // print Header
    {
        char temp[100];
        for (int i = 0; i < 9; ++i) {
            numMatch = fscanf(fp, "%s", temp);
        }
    }

    int totalAgi = 0;
    int iteration = 0;
    while (iteration < 2600) {
        char firstField[100];
        numMatch = fscanf(fp, "%s", firstField);
        // printf("%d %s\n", numMatch, field);
        if (strcmp(firstField, "\"25\"") == 0) {
            // get past first couple of columns
            char field[100];
            for (int i = 0; i < 4; ++i) {
                numMatch = fscanf(fp, "%s", field);
            }

            // get stateDest
            char stateDest[100];
            numMatch = fscanf(fp, "%s", stateDest);
            char * underscore = strchr(stateDest, '_');
            if (underscore != NULL) {
                *underscore = ' ';
                char * secondUnderscore = strchr(stateDest, '_');
                if (secondUnderscore != NULL) {
                    *secondUnderscore = ' ';
                }
            }

            // get Aggr_AGI and print line in report
            int aggrAgi = 0;
            for (int i = 0; i < 3; ++i) {
                numMatch = fscanf(fp, "%d", &aggrAgi);
                if (i == 2) {
                    printf("%-*s%d\n", width, stateDest, aggrAgi);
                }
            }
            totalAgi += aggrAgi;
        } else {
            // jump to the next line
            char dump[1000];
            fgets(dump, 1000, fp);
        }
        ++iteration;
    }
    // print final lines and total in the report.
    printf("---------------------------------\n");
    printf("%-*s%d\n", width, "Total", totalAgi);
    fclose(fp);
	return 0;
}
